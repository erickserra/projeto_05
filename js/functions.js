$(function(){
	
	//slider depoimentos - pagina index


	var amtDepoimento = $('.quote').length;
	var indiceAtual = 0;
	iniciarDep();
	alternarDep();

	function iniciarDep(){
		$('.quote').hide();
		$('.quote').eq(indiceAtual).show();
	}


	function alternarDep(){
		$('[next]').click(function(){
			if (indiceAtual < amtDepoimento-1) {
				indiceAtual++;
				$('.quote').hide();
				$('.quote').eq(indiceAtual).show();
			} else{
				indiceAtual = 0;
				$('.quote').hide();
				$('.quote').eq(indiceAtual).show();
			}
		});
		$('[prev]').click(function(){
			if (indiceAtual > 0) {
				indiceAtual--;
				$('.quote').hide();
				$('.quote').eq(indiceAtual).show();
			} else{
				indiceAtual = 0;
				$('.quote').hide();
				$('.quote').eq(indiceAtual).show();
			}
		});

	}
	
	//pagina de venda -> barra-preco
	
	var currentValue = 0;

	var isDrag = false;
	var preco_maximo = 70000;
	var preco_atual = 0;

	$('.pointer-barra').mousedown(function(){
		isDrag = true;
		console.log('pressionado');
	});

	$(document).mouseup(function(){
		EnableTextSelection();
		isDrag = false;
	});

	$('.barra-preco').mousemove(function(e){
		if (isDrag == true) {
			disableTextSelection();
			var elBase = $(this);
			var mouseX = e.pageX - elBase.offset().left;

			if (mouseX < 0) {mouseX = 0;}
			if (mouseX > elBase.width()) {mouseX = elBase.width();}


			$('.pointer-barra').css('left',(mouseX-9)+'px');
			var currentValue = (mouseX / elBase.width()) * 100;
			$('.barra-preco-fill').css('width',currentValue+'%');

			preco_atual = (currentValue / 100) * preco_maximo;
			preco_atual = formatarPreco(preco_atual);
			$('.preco_atual').html('R$'+preco_atual);
		}
	});

	$(window).resize(function(){
		$('.pointer-barra').css('left',0);
		$('.barra-preco-fill').css('width',0);
	});	

	function disableTextSelection(){
		$("body").css("-webkit-user-select","none");
		$("body").css("-moz-user-select","none");
		$("body").css("-ms-user-select","none");
		$("body").css("-o-user-select","none");
		$("body").css("user-select","none");
	}

	function EnableTextSelection(){
		$("body").css("-webkit-user-select","auto");
		$("body").css("-moz-user-select","auto");
		$("body").css("-ms-user-select","auto");
		$("body").css("-o-user-select","auto");
		$("body").css("user-select","auto");
	}
	
	function formatarPreco(preco_atual){
		preco_atual = preco_atual.toFixed(2);
		preco_array = preco_atual.split('.');

		var novo_preco = formatarTotal(preco_array);

		return novo_preco;
	}

	function formatarTotal(preco_array){
		if (preco_array[0] < 1000) {
			return (preco_array[0]+','+preco_array[1]);
		} else if(preco_array[0] < 10000){
			return preco_array[0][0]+'.'+preco_array[0].substr(1,preco_array[0].length)+','+preco_array[1];
		} else {
			return preco_array[0][0] + preco_array[0][1] +'.'+preco_array[0].substr(2,preco_array[0].length)+','+preco_array[1];
		}
	}
	

	// pagina individual de venda
	//slider

	//background-image: url('imagens/carro2.jpg')



	var imgShow = 3;
	var individual = 33.3;
	var maxIndex = Math.ceil($('.mini-img-wraper').length/3) - 1;
	var curIndex = 0;


	initSlider();
	navigateSlider();
	clickSlider();

	function initSlider(){
		var img = $('.mini-img-wraper').children(0).css('background-image');
		$('.foto-destaque').css('background-image',img);
		var amt = $('.mini-img-wraper').length * individual;
		var elScroll = $('.nav-galeria-wraper');
		var elSingle = $('.mini-img-wraper');
		elScroll.css('width',amt+'%');
		elSingle.css('width',individual*(100/amt)+'%');
	}

	function navigateSlider(){
		$('.arrow-right-parent').click(function(){
			if (curIndex < maxIndex) {
				curIndex++;
				var elOff = $('.mini-img-wraper').eq(curIndex*3).offset().left - $('.nav-galeria-wraper').offset().left;
				$('.nav-galeria').animate({'scrollLeft':elOff+'px'});
			}
		});

		$('.arrow-left-parent').click(function(){
			if (curIndex > 0) {
				curIndex--;
				var elOff = $('.mini-img-wraper').eq(curIndex*3).offset().left - $('.nav-galeria-wraper').offset().left;
				$('.nav-galeria').animate({'scrollLeft':elOff+'px'});
			}
		});
	}



	function clickSlider(){
			$('.mini-img-wraper').click(function(){
				$('.mini-img-wraper').css('background-color','transparent');
				$(this).css('background-color','rgb(210,210,210)');

				var img = $(this).children().css('background-image');
				$('.foto-destaque').css('background-image',img);
			});
		}


		//click button contato

		var directory = '/Projetos/Projeto_05/';

		$('[goto=contato]').click(function(){
			location.href=directory+'index.html?contato';
			return false;
		});

		checkUrl();

		function checkUrl(){

			var url = location.href.split('/');
			var curPage = url[url.length-1].split('?');

			if (curPage[1] != undefined && curPage[1] == 'contato') {

				$('header nav a').css('color','black');
				$('footer nav a').css('color','white');
				$('[goto=contato]').css('color','#EB2D2D');
				var elOff = $('#contato').offset().top;
				$('html,body').animate({'scrollTop':elOff});

				return false;
			}
		}		

});